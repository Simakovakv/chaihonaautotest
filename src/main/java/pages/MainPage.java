package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MainPage extends BasePage{
    @FindBy(xpath="//*[@id='desktopMenuMain']//a[contains(text(), 'Меню доставки')]")
    WebElement menuButton;

    @FindBy(xpath="//*[@id='desktopMenuMain]//*[@id='desktopMenuMain']//a[@href='/catalog']/..//div[@class='main-menu__wrap']/ul[contains(@class, 'main-menu__list')]")
    WebElement menuItems;

    private static final String menuFormat = "//*[@id='desktopMenuMain']//li[@class='level-2__item']/a[contains(text(),'%s')]";

    public MainPage(WebDriver driver){
        super(driver);
    }
    public void clickMenu(String itemName){
        By menuLocator = By.xpath(String.format(menuFormat, itemName));
        //List<WebElement> href = driver.findElements(By.xpath("//*[@id='desktopMenuMain']//li[@class='level-2__item']/a[contains(text(),'"+text+"')]"));
        List<WebElement> href = driver.findElements(menuLocator);
        assertNotEquals(String.format("%s item in menu is not found", itemName), href.size(), 0 );
        this.click(href.get(0));
    }
    @Override
    public boolean isPageLoaded() {
        return false;
    }
}
