package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;

public abstract class BasePage {
    public abstract boolean isPageLoaded();
    WebDriver driver;
    public BasePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void click(WebElement element){
        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }
    public void scrollTo(WebElement element){
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", element);
    }
    public void waitOf(Function func){
        new WebDriverWait(driver, 5).until(func);
    }
    public void setText(String text){}
     public WebDriver getDriver(){
        return driver;
    }
    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
